
class UserResponse {
  bool success;
  String message;
  UserData data;
  UserResponse(this.success, this.message, this.data);
  UserResponse.fromJson(Map<String, dynamic> map):
        success = map['success'],
        message = map['message'],
        data = UserData.fromJson(map['data']);

  @override
  String toString() {
    return 'UserResponse{success: $success, message: $message, data: $data}';
  }
}

class UserData {
  UserModel user;
  UserData(this.user);
  UserData.fromJson(Map<String, dynamic> map): user = UserModel.fromJson(map['user']);

  @override
  String toString() {
    return 'UserData{user: $user}';
  }
}

class UserModel {
  String id;
  String email;
  String username;
  String fullName;
  String createdAt;
  String updatedAt;
  UserModel(this.id, this.email, this.username, this.fullName, this.createdAt,
      this.updatedAt);
  UserModel.fromJson(Map<String, dynamic> map):
        id = map['id'],
        email = map['email'],
        username = map['username'],
        fullName = map['full_name'],
        createdAt = map['created_at'],
        updatedAt = map['updated_at'];

  @override
  String toString() {
    return 'UserModel{id: $id, email: $email, username: $username, fullName: $fullName, createdAt: $createdAt, updatedAt: $updatedAt}';
  }
}