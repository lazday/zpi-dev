
class AuthResponse {
  String tokenType;
  String accessToken;
  String refreshToken;
  int expiresIn;
  AuthResponse( this.tokenType, this.accessToken, this.refreshToken, this.expiresIn);
  AuthResponse.fromJson(Map<String, dynamic> map):
        tokenType = map['token_type'],
        accessToken = map['access_token'],
        refreshToken = map['refresh_token'],
        expiresIn = map['expires_in'];

  @override
  String toString() {
    return 'AuthResponse{tokenType: $tokenType, accessToken: $accessToken, refreshToken: $refreshToken, expiresIn: $expiresIn}';
  }
}