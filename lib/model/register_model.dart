import 'package:zpi_dev/model/user_model.dart';

class RegisterResponse {
  bool success;
  String message;
  RegisterData data;
  RegisterResponse(this.success, this.message, this.data);
  RegisterResponse.fromJson(Map<String, dynamic> map):
        success = map['success'],
        message = map['message'],
        data = RegisterData.fromJson(map['data']);

  @override
  String toString() {
    return 'RegisterResponse{success: $success, message: $message, data: $data}';
  }
}

class RegisterData {
  UserModel user;
  RegisterData(this.user);
  RegisterData.fromJson(Map<String, dynamic> map): user = UserModel.fromJson(map['user']);

  @override
  String toString() {
    return 'RegisterData{user: $user}';
  }
}