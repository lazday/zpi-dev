
import 'package:zpi_dev/preferences/pref_instance.dart';

class PrefManager {

  static Future<int> get getIsLogin => PrefInstance.getInt( PrefName.isLogin );
  static Future setIsLogin(int value) => PrefInstance.setInt(PrefName.isLogin, value);

  static Future<String> get getUserToken => PrefInstance.getString(PrefName.userToken);
  static Future setUserToken(String value) => PrefInstance.setString(PrefName.userToken, value);

  static Future logout() => PrefInstance.setInt(PrefName.isLogin, 0);
}


class PrefName {
  static String isLogin = "pref_is_login";
  static String userToken = "pref_user_token";
}