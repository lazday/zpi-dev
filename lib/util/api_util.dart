
class ApiUtil {
  static final String url = "https://zpi-dev.zicare.id/";
  static Uri baseUrl (String endpoint) {
    return Uri.parse( url + endpoint );
  }
}