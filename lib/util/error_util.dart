import 'dart:convert';

import 'package:http/http.dart';
import 'package:zpi_dev/model/error_model.dart';

class ErrorUtil {

  static String message(Response response) {
    if (response.statusCode == 400) {
      var error = ErrorResponse.fromJson( json.decode(response.body) );
      return error.message;
    }
    return "Terjadi kesalahan";
  }
}