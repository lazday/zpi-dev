import 'package:flutter/material.dart';
import 'package:zpi_dev/ui/login.dart';

import 'home.dart';
import 'package:zpi_dev/preferences/pref_manager.dart' as pref;

class Splashscreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    pref.PrefManager.getIsLogin.then((value) {
      Future.delayed(Duration(seconds: 3), () {
        Navigator.of(context) .pushReplacement(
            MaterialPageRoute(builder: (BuildContext context) {
              if (value == 1) return Home();
              else return Login();
            })
        );
      });
    });
    
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Container (
          width: 200,
          height: 200,
          child: Image.asset('images/flutter.png'),
        ),
      ),
    );
  }
}
