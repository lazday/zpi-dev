import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:zpi_dev/model/error_model.dart';
import 'package:zpi_dev/model/user_model.dart';
import 'package:zpi_dev/preferences/pref_manager.dart' as pref;
import 'package:zpi_dev/ui/login.dart';
import 'package:http/http.dart' as http;
import 'package:zpi_dev/util/api_util.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  String fullName = "";
  String username = "";
  String email = "";
  bool isLoading = false;

  @override
  void initState() {
    pref.PrefManager.getUserToken.then((value) => getUser( value ) );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding (
        padding: EdgeInsets.all(20),
        child: isLoading ? Container() : Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              child: Text(
                "Full Name ",
                style: TextStyle(fontSize: 16, color: Colors.black54),
                textAlign: TextAlign.left,
              ),
            ),
            Container(
              child: Text(
                fullName ,
                style: TextStyle(fontSize: 24, color: Colors.black45, fontWeight: FontWeight.bold),
                textAlign: TextAlign.left,
              ),
            ),
            Container(
              child: Text(
                "Username ",
                style: TextStyle(fontSize: 16, color: Colors.black54),
                textAlign: TextAlign.left,
              ),
            ),
            Container(
              child: Text(
                username ,
                style: TextStyle(fontSize: 24, color: Colors.black45, fontWeight: FontWeight.bold),
                textAlign: TextAlign.left,
              ),
            ),
            Container(
              child: Text(
                "Email ",
                style: TextStyle(fontSize: 16, color: Colors.black54),
                textAlign: TextAlign.left,
              ),
            ),
            Container(
              child: Text(
                email ,
                style: TextStyle(fontSize: 24, color: Colors.black45, fontWeight: FontWeight.bold),
                textAlign: TextAlign.left,
              ),
            ),
            Container (
                margin: EdgeInsets.only(top: 20),
                width: double.infinity,
                child: MaterialButton(
                    height: 50,
                    color: Colors.white,
                    shape: RoundedRectangleBorder (
                      side: BorderSide(color: Colors.blueAccent, width: 2, style: BorderStyle.solid),
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Text('Sign Out', style: TextStyle (color: Colors.blueAccent, fontSize: 18),),
                    onPressed: () {
                      pref.PrefManager.logout().then((value) {
                        Navigator.of(context) .pushReplacement(
                            MaterialPageRoute(builder: (BuildContext context) => Login())
                        );
                      });
                    }
                )
            ),
          ],
        ),
      ),
    );
  }

  Widget setLoading(){
    return Padding(
      padding: EdgeInsets.all(10),
      child: Container (
        width: 50,
        height: 50,
        child: CircularProgressIndicator(),
      ),
    );
  }

  void getUser(String token) async {
    setState(() => isLoading = true );
    final response = await http.get(
        ApiUtil.baseUrl( "api/v1/users/me/" ),
        headers: { 'Authorization': 'Bearer $token', }
    );
    var result = jsonDecode( response.body );
    print('response $result');
    if (response.statusCode == 200) {
      setState(() => isLoading = false );
      var user = UserResponse.fromJson(result);
      setState(() {
        fullName = user.data.user.fullName;
        username = user.data.user.username;
        email = user.data.user.email;
      });
    } else {
      setState(() => isLoading = false );
      var error = ErrorResponse.fromJson(result);
      Fluttertoast.showToast(msg: error.message);
    }
  }
}
