import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zpi_dev/model/auth_model.dart';
import 'package:zpi_dev/model/error_model.dart';
import 'package:zpi_dev/ui/home.dart';
import 'package:zpi_dev/ui/register.dart';
import 'package:zpi_dev/util/api_util.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:zpi_dev/preferences/pref_manager.dart' as pref;

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {

  String username = "";
  String password = "";
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding (
        padding: EdgeInsets.all(20),
        child: SingleChildScrollView (
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  margin: EdgeInsets.only(top: 40),
                  child: Text(
                    'Sign In',
                    style: TextStyle(fontSize: 38, color: Colors.black45, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.left,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 5, right: 5, top: 20),
                child: TextFormField(
                  style: TextStyle(fontSize: 18),
                  decoration: InputDecoration (
                    border: UnderlineInputBorder (),
                    labelText: 'Username',
                  ),
                  onChanged: (String text){
                    username = text;
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 5, right: 5, top: 10),
                child: TextFormField(
                  obscureText: true,
                  style: TextStyle(fontSize: 18),
                  decoration: InputDecoration (
                    border: UnderlineInputBorder (),
                    labelText: 'Password',
                  ),
                  onChanged: (String text){
                    password = text;
                  },
                ),
              ),
              Container (
                margin: EdgeInsets.only(top: 30),
                child: isLoading ? setLoading() : setButton(),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    padding: EdgeInsets.all(5),
                    child: Text(
                      "Don't have account?",
                      style: TextStyle(fontSize: 16, color: Colors.black54),
                      textAlign: TextAlign.left,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    padding: EdgeInsets.all(5),
                    child: InkWell(
                      child: Text(
                        'Sign Up',
                        style: TextStyle(fontSize: 16, color: Colors.blueAccent, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.left,
                      ),
                      onTap: (){
                        Navigator.of(context) .pushReplacement(
                            MaterialPageRoute(builder: (BuildContext context) => Register())
                        );
                      },
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget setButton(){
    return Container(
      width: double.infinity,
      child: MaterialButton(
          height: 50,
          color: Colors.blueAccent,
          shape: RoundedRectangleBorder (
            borderRadius: BorderRadius.circular(20),
          ),
          child: Text('Sign In', style: TextStyle (color: Colors.white, fontSize: 18),),
          onPressed: () {
            if(isRequired()) login(context);
          }
      )
    );
  }

  Widget setLoading(){
    return Padding(
        padding: EdgeInsets.all(10),
        child: Container (
          width: 50,
          height: 50,
          child: CircularProgressIndicator(),
        ),
    );
  }

  bool isRequired(){
    if (username.isEmpty) {
      Fluttertoast.showToast(msg: 'Username is required');
      return false;
    } else if (password.isEmpty) {
      Fluttertoast.showToast(msg: 'Password is required');
      return false;
    }
    return true;
  }

  void login(BuildContext context) async {
    setState(() => isLoading = true );
    final response = await http.post(
        ApiUtil.baseUrl( "api/v1/auth/token" ),
        body: jsonEncode(
            {
              "username": username,
              "password": password,
              "grant_type": "password"
            }
        )
    );
    var result = jsonDecode( response.body );
    print('response $result');
    if (response.statusCode == 200) {
      setState(() => isLoading = false );
      var auth = AuthResponse.fromJson(result);
      pref.PrefManager.setIsLogin(1);
      pref.PrefManager.setUserToken(auth.accessToken);
      Navigator.of(context) .pushReplacement(
          MaterialPageRoute(builder: (BuildContext context) => Home())
      );
    } else {
      setState(() => isLoading = false );
      var error = ErrorResponse.fromJson(result);
      Fluttertoast.showToast(msg: error.message);
    }
  }

}
